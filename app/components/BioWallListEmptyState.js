import RaisedButton from 'material-ui/RaisedButton';
import  React from 'react';
import store from '../store';


const BioWallListEmptyState = ({ onAddBioWall }) => (
  <div style={{ textAlign: 'center', marginTop: 24 }}>
    {store.role === 'admin' ?
      <div>
        <h3>You don't have any BioWall, please add biowall.</h3>
        <RaisedButton label="+ Add one" onTouchTap={onAddBioWall} backgroundColor={'#95A105'} style={{ marginTop: 16 }}/>
      </div>
      :
      <div>
        <h3>You don't have any BioWall, please contact administrator to add biowall.</h3>
      </div>
    }

  </div>
);

BioWallListEmptyState.propTypes = {
  onAddBioWall: React.PropTypes.func.isRequired,
};

export default BioWallListEmptyState;
