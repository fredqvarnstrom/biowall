import React from 'react';
import BioWallListItem from './BioWallListItem';
import { withRouter } from 'react-router';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import api from '../api';


class BioWallListItemContainer extends React.Component {

  constructor(props) {
    super(props);

    let open_dialog = false;
    let txt_dialog = '';
    let btn_dialog = false;
    let isWorking = false;
    let action_type = null;
    this.state = {
      open_dialog,
      txt_dialog,
      btn_dialog,
      isWorking,
      action_type,
    };
  }

  onTouchBioWallItem(){
    /*
    When user touches the whole BioWall Item, move to its detailed page.
     */
    let serialnumber = this.props.biowall.serialnumber;
    this.props.router.push('/biowalls/' + serialnumber);
  }

  handleCloseDialog(){
    this.setState({open_dialog: false, txt_dialog:''})
  }

  onEventDelete(){
    this.setState({open_dialog: true, action_type: 'delete'})
  }

  onEventEdit(){
    this.setState({open_dialog: true, action_type: 'edit', txt_dialog: this.props.biowall.name})
  }

  performAction(){
    this.setState({isWorking: true});
    if (this.state.action_type == 'delete'){
      api.removeBioWall(this.props.biowall.serialnumber)
        .then(response => {
          if (response.payload.status_code == 204){
            console.log("Successfully deleted...");
          }
          this.setState({isWorking: false});
          this.handleCloseDialog()
        })
        .catch(err => {
          console.log("Error");
          console.log(err.toString());
          this.setState({isWorking: false});
      });
    }
    else{
      api.updateBioWallName(this.props.biowall.serialnumber, this.state.txt_dialog)
        .then(response => {
          if (response.payload.status_code == 204){
            console.log("Successfully updated...");
          }
          this.setState({isWorking: false});
          this.handleCloseDialog()
        })
        .catch(err => {
          console.log("Error");
          console.log(err.toString());
          this.setState({isWorking: false});
      });
    }

  }

  onChangeTextDialog(event){
    let new_val = event.target.value;
    this.setState({txt_dialog: new_val});
    if (this.state.action_type == 'delete')
      if (new_val == 'DELETE'){
        this.setState({btn_dialog: true});
      }
      else {
        this.setState({btn_dialog: false});
      }
    else
      if (new_val.length > 0){
        this.setState({btn_dialog: true});
      }
      else {
        this.setState({btn_dialog: false});
      }
  }

  render() {
    const { name, serialnumber } = this.props.biowall;
    const actions_dialog = [
      <FlatButton
        label="CANCEL"
        style={{color: "#95A105"}}
        keyboardFocused={true}
        disabled={this.state.isWorking}
        onTouchTap={this.handleCloseDialog.bind(this)}
      />,
      <FlatButton
        label={this.state.action_type === 'delete' ? "DELETE" : "APPLY"}
        style={{color: "#95A105"}}
        disabled={!this.state.btn_dialog || this.state.isWorking}
        onTouchTap={this.performAction.bind(this)}
      />,
    ];

    return (
      <div>
        <BioWallListItem
          name={name}
          serialNumber={serialnumber}
          onTouchThis={this.onTouchBioWallItem.bind(this)}
          onDeleteThis={this.onEventDelete.bind(this)}
          onEditThis={this.onEventEdit.bind(this)}
        />
        <Dialog
          title={this.state.action_type === 'delete' ? "Delete Device" : "Device Name"}
          actions={actions_dialog}
          modal={false}
          open={this.state.open_dialog}
          onRequestClose={this.handleCloseDialog.bind(this)}
        >
          {this.state.action_type === 'delete' ?
            <div>WARNING: This action cannot be undone. The device {name} will be deleted permanently.</div>
            : <div>Change device name</div>}

          <br/>
          <TextField
            hintText={this.state.action_type === 'delete' ? "Type 'DELETE' to confirm" : "Device name"}
            floatingLabelText={this.state.action_type === 'delete' ? "Type 'DELETE' to confirm" : "Device name"}
            value={this.state.txt_dialog}
            underlineStyle={{borderColor: '#95A105'}}
            underlineFocusStyle={{borderColor: '#95A105' }}
            hintStyle={{color: '#95A105'}}
            floatingLabelFocusStyle={{color: '#95A105'}}
            onChange={this.onChangeTextDialog.bind(this)}
            disabled={this.state.isWorking}
          />
        </Dialog>
      </div>
    );
  }
}

BioWallListItemContainer.propTypes = {
  biowall: React.PropTypes.object.isRequired,
  router: React.PropTypes.shape({
    push: React.PropTypes.func.isRequired
  })
};

export default withRouter(BioWallListItemContainer)