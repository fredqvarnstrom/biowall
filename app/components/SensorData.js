import React from 'react';
import api from '../api';
import {grey500 , black } from 'material-ui/styles/colors';
import LoadingIndicator from '../components/LoadingIndicator';
import MessageBox from '../components/MessageBox';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Chart} from 'react-google-charts';
import Slider from 'material-ui/Slider';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';
import DatePicker from 'material-ui/DatePicker';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';

const styles = {
  title: {
    cursor: 'pointer',
  },
  appbar: {
    backgroundColor: '#95A105',
  },
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 10,
  },
  table: {
    fontSize: 30,
  }
};

export default class SensorData extends React.Component {

  constructor(props) {
    super(props);
    let errorText = null;
    let values = null;
    let slider = 300;
    let itemCount = 200;
    let day_offset = 1;
    let date = new Date();
    date.setHours(0, 0, 0, 0);
    if (window.innerWidth < 500)
      itemCount = 50;
    this.state = {
      errorText,
      values,
      slider,
      itemCount : itemCount,
      day_offset,
      date,
    };
  }

  componentWillMount() {
    this.mounted = true;
    this.pollSensorData();
  }

  componentWillUnmount() {
    this.mounted = false;
    clearTimeout(this.state.timeoutId);
  }

  pollSensorData() {
    let start_time = this.state.date.toISOString();
    let tmp = new Date();
    tmp.setHours(0, 0, 0, 0);
    let end_time = null;
    // Today?
    if (this.state.date.getTime() != tmp.getTime()){
      tmp.setDate(this.state.date.getDate() + 1);
      tmp.setHours(0, 0, 0, 0);
      end_time = tmp.toISOString();
    }

    // console.log(start_time);
    // console.log(end_time);
    api.getBioWallData(this.props.sn, this.props.alias, this.state.itemCount, 'sensor', 301-this.state.slider, start_time, end_time)
      .then(response => this.handleBioWallApiResponse(response))
      .catch(err => {
        clearTimeout(this.state.timeoutId);
        if (!this.mounted) return;
        this.setState({
          errorText: err.toString(),
          values: null,
          timeoutId: null,
        })
      });
  }

  handleBioWallApiResponse(response) {
    // console.log(response);
    if (!this.mounted) return;
    // Re-draw every 10 sec
    const timeoutId = setTimeout(() => this.pollSensorData(), 10000);
    const val_list = response.payload;
    // console.log(val_list);
    if (response.status === 304)
      this.setState({
        errorText: null,
        timeoutId
      });
    else{
      this.setState({
        errorText: null,
        values: val_list,
        timeoutId
      });
    }
  }

  handleSlider(event, value){
    // console.log('New Slide value: ', value);
    this.setState({slider: value});
    setTimeout(() => this.redrawChart(), 1000);
  };

  redrawChart(){
    this.setState({values: null});
    this.pollSensorData();
  }

  handleFloatButton(val){
    let new_slider = 0;
    if (val == 'add')
      new_slider = this.state.slider + 15;
    else
      new_slider = this.state.slider - 15;
    if (new_slider >= 0 && new_slider <= 300)
      this.handleSlider(null, new_slider);
  }

  handleChangeDate(event, date){
    this.setState({date: date, values: null});
    // this.pollSensorData();
  }

  change_day(val){
    let tmp = this.state.date;
    tmp.setDate(tmp.getDate() + val);
    this.setState({date: tmp, values: null});
  }
  renderErrorMessage() {
    return (
      <div className="container container--space">
        <MessageBox error text={this.state.errorText} />
      </div>
    );
  }

  renderMainContent() {
    if (this.state.errorText) return this.renderErrorMessage();
    if (this.state.values == null) return <LoadingIndicator />;
    // Convert Epoch seconds value to date value
    // Sample value: [1480572566, "5.75", "", ""]   >>>>  [timestamp, value, user_mail, action]
    let chart_data = [];
    let table_data = [];
    if (this.state.values.length > 0) {
      chart_data = this.state.values.map((val) => {
        return [epoch_to_date(val[0], true), parseFloat(val[1])]
      });
      chart_data = [['Timestamp', this.props.alias],].concat(chart_data);
      table_data = this.state.values.map((val) => {
        return [epoch_to_date(val[0]).toString(), val[1]]
      });
      table_data = table_data.filter(dd => dd[1] != null);
    }
    return (
      <div style={{backgroundColor: '#CCCCCC'}}>
        <div className={"my-pretty-chart-container"}>
            {
            chart_data.length > 1 ?
              <table>
                <tr>
                  <td width="50px">
                    <IconButton tooltip="Previous Day" onClick={(val) => this.change_day(-1)}>
                      <FontIcon className="material-icons" style={{margin: 5}} color={black}>skip_previous</FontIcon>
                    </IconButton>
                  </td>
                  <td style={{width: window.innerWidth - 100}}>
                    <Chart
                        chartType="LineChart"
                        data={chart_data}
                        options={{interpolateNulls: true}}
                        graph_id="ScatterChart"
                        width="100%"
                        height="400px"
                        chartPackages={['corechart', 'timeline']}
                        legend_toggle
                    />
                    <div>
                      <table style={{borderSpacing: 10}}>
                        <tr>
                          <td>
                            <FloatingActionButton mini={true} backgroundColor="#95A105" onClick={(val) => this.handleFloatButton('minus')}>
                              <ContentRemove/>
                            </FloatingActionButton>
                          </td>
                          <td>
                            <Slider
                              style={{width: window.innerWidth - 300}}
                              defaultValue={0}
                              min={0}
                              max={300}
                              step={5}
                              value={this.state.slider}
                              onChange={(event, value) => this.handleSlider(event, value)}
                            />
                          </td>
                          <td>
                            <FloatingActionButton mini={true} backgroundColor="#95A105" onClick={(val) => this.handleFloatButton('add')}>
                              <ContentAdd />
                            </FloatingActionButton>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                  <td width="50px">
                    <IconButton tooltip="Next Day" onClick={(val) => this.change_day(1)}>
                      <FontIcon className="material-icons" style={{margin: 5}} color={black}>skip_next</FontIcon>
                    </IconButton>
                  </td>
                </tr>
              </table>
              :
              <h3>No Data</h3>
            }
          <div style={{position: 'absolute', left: "50%", marginLeft: -100, top: 450}}>
            <table style={{width: 100}}>
              <tr>
              <td>
                <FontIcon className="material-icons" style={{margin: 5}} color={black}>date_range</FontIcon>
              </td>
              <td>
                <DatePicker inputStyle={{color: black}}
                            textFieldStyle={{width: 80}}
                            hintText="Today" defaultDate={this.state.date}
                            onChange={(event, date) => this.handleChangeDate(event, date)}/>
              </td>
              </tr>
            </table>
          </div>
        </div>
        <Table>
          <TableHeader displaySelectAll={false} style={{background: '#AA3F00'}}>
            <TableRow>
              <TableHeaderColumn style={{fontSize: 20, width: '70%', textAlign:'center', paddingLeft: 0}}>Timestamp</TableHeaderColumn>
              <TableHeaderColumn style={{fontSize: 20, width: '30%', textAlign:'center', paddingLeft: 0}}>Reading</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody showRowHover displayRowCheckbox={false}>
            {table_data.map((row_data, i) =>
              <TableRow key={i} style={{backgroundColor: '#A5D6A7'}}>
                <TableRowColumn style={{width: '70%', textAlign:'center'}}>{row_data[0]}</TableRowColumn>
                <TableRowColumn style={{width: '30%', textAlign:'center'}}>{row_data[1]}</TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    );
  }
  render() {
    return (
      <div>
        {this.renderMainContent()}
      </div>
    );
  }
}

SensorData.propTypes = {
  sn: React.PropTypes.string.isRequired,
  alias: React.PropTypes.string.isRequired,
};

SensorData.defaultProps = {
  sn : '000001',
  alias: 'PH',
};


function epoch_to_date(sec, rfc) {
  let d = new Date(0); // The 0 there is the key, which sets the date to the epoch
  d.setUTCSeconds(sec);
  let result = null;
  if (rfc == true)
    result = d;
  else
    if (window.innerWidth < 600)
      result = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
    else
      result = d;
  return result
}