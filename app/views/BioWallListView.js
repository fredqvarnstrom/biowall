import React from 'react';
import api from '../api';
import MessageBox from '../components/MessageBox';
import FAB from '../components/FAB';
import LoadingIndicator from '../components/LoadingIndicator';
import AddBioWallModal from '../components/AddBioWallModal';
import BioWallList from '../components/BioWallList';
import BioWallListEmptyState from '../components/BioWallListEmptyState';
import NavBar from '../components/NavBar';
import AppBar from 'material-ui/AppBar';
import store from '../store';
import RaisedButton from 'material-ui/RaisedButton';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import ChevronRightIcon from 'material-ui/svg-icons/navigation/chevron-right';
import { Link } from 'react-router';
import NotificationSystem from 'react-notification-system';

const HA_POLL_INTERVAL_MS = 1000;

export default class BioWallListView extends React.Component {
  /**
   * constructor() is where you initialize the react state. By convention it is
   * the first method defined in any JavaScript class.
   */
  constructor(...args) {
    super(...args);

    this.state = {
      addingBioWallErrorText: null,
      errorText: null,
      isAddingBioWall: false,
      isAddBioWallModalOpen: false,
      biowalls: store.biowalls || null,
      timeoutId: null,
    };
  }
  componentWillMount() {
    this.mounted = true;
    this.pollBioWalls();
  }
  componentWillUnmount() {
    this.mounted = false;
    clearTimeout(this.state.timeoutId);
  }

  componentDidMount() {
    if (this.state.biowalls == null)
      return;
    let alert_msg = [];

    for (var i=0; i < this.state.biowalls.length; i++){
      let wall = this.state.biowalls[i];
      if (wall.alert != null)
        alert_msg.push({'device': wall.name, 'msg': wall.alert})
    }
    if (alert_msg.length > 0)
      this.refs['notificationSystem'].addNotification({
        title: 'BioWall Notification',
        level: 'error',
        dismissible: false,
        autoDismiss: 0,
        position: 'br',
        uid: 100,
        children: (
          <div style={{margin:10}}>
            {
              alert_msg.map((a, i) =>
                <div>
                  <li>{a.device}</li>
                  <span style={{marginLeft: 15}}>{a.msg}</span>
                  <br/><br/>
                </div>
              )}
            <RaisedButton onTouchTap={this.dismissNotify.bind(this)} backgroundColor={'#95A105'}>
              I Got it
            </RaisedButton>
          </div>
        ),
      });
  }
  dismissNotify(){
    for (var i=0; i < this.state.biowalls.length; i++){
      if (this.state.biowalls[i].alert != null)
        api.dismissAlert(this.state.biowalls[i].serialnumber);
    }
    this.refs['notificationSystem'].removeNotification(100);
  }

  pollBioWalls() {
    api.getBioWalls()
      .then(response => {
        if (!this.mounted) return;
        const timeoutId = setTimeout(() => this.pollBioWalls(), HA_POLL_INTERVAL_MS);
        if (response.status === 304) {
          this.setState({ timeoutId });
        } else {
          store.biowalls = response.payload;
          this.setState({
            biowalls: response.payload,
            timeoutId,
          });

        }
      })
      .catch(err => {
        clearTimeout(this.state.timeoutId);
        if (!this.mounted) return;
        store.biowalls = null;
        this.setState({
          errorText: err.toString(),
          biowalls: null,
          timeoutId: null,
        });
      });
  }

  /**
   * Set the state so this component re-renders and the modal opens
   */
  openAddBioWallModal() {
    this.setState({ isAddBioWallModalOpen: true });
  }

  /**
   * Set the state so the component re-renders and the modal closes
   */
  closeAddBioWallModal() {
    this.setState({ isAddBioWallModalOpen: false });
  }

  renderMainContent() {
    const { errorText, biowalls } = this.state;
    if (errorText) return <MessageBox error text={errorText} />;
    if (!biowalls) return <LoadingIndicator />;
    if (biowalls.length) return <BioWallList biowalls={biowalls} />;
    return <BioWallListEmptyState onAddBioWall={() => this.openAddBioWallModal()} />;
  }

  render() {
    return (
      <div>
        <NavBar />
        <AppBar style={{background: '#95A105'}} title="My BioWalls" iconElementLeft={<span/>}/>
        <div className="container" style={{marginTop: 10}}>
          {this.renderMainContent()}
        </div>
        <AddBioWallModal
          isOpen={this.state.isAddBioWallModalOpen}
          isAdding={this.state.isAddingBioWall}
          onAdd={(name, serialNumber) => this.addBioWall(name, serialNumber)}
          onRequestClose={() => this.closeAddBioWallModal()}
        />
        {store.role === 'admin' ? <FAB onTouchTap={() => this.openAddBioWallModal()} /> : null}
        <NotificationSystem ref="notificationSystem"/>
      </div>
    );
  }
}
