import React from 'react';
import { hashHistory, browserHistory } from 'react-router';
import api from '../api';
import MessageBox from '../components/MessageBox';
import BioWallDetail from '../components/BioWallDetail';
import LoadingIndicator from '../components/LoadingIndicator';
import NavBar from '../components/NavBar';
import store from '../store';
import RaisedButton from 'material-ui/RaisedButton';
import NotificationSystem from 'react-notification-system';
import { withRouter } from 'react-router';

// Special error for web developers to let them know something is wrong with
// their Murano configuration.
function getMuranoErrorText() {
  return `Murano Error: It appears this serial number was either not
    added as a device, this device was not activated, the product was
    not associated with this solution, or the device has not written
    to the platform.`;
}

class BioWallView extends React.Component {
  constructor(props) {
    super(props);

    let biowall = null;
    let errorText = null;
    let start_time = (new Date).getTime();
    if (store.biowalls) {
      biowall = store.biowalls.filter(wall => wall.serialnumber == props.params.serialnumber)[0];
      if (biowall && (biowall.state === null || !biowall.hasOwnProperty('state') || biowall.state === "undefined")) {
        biowall = null;
        errorText = getMuranoErrorText();
      }
    }

    let b_listview = false;

    this.state = {
      errorText,
      isChangingWallState: false,
      biowall,
      start_time,
      b_listview,
    };
  }

  componentWillMount() {
    this.mounted = true;
    this.pollBioWalls();
  }

  componentWillUnmount() {
    this.mounted = false;
    clearTimeout(this.state.timeoutId);
  }

  componentDidMount() {
    if (this.state.biowall != null)
      if (this.state.biowall.alert != null)
        this.refs['notificationSystem'].addNotification({
          title: 'BioWall Notification',
          message: this.state.biowall.alert,
          level: 'error',
          dismissible: false,
          position: 'br',
          autoDismiss: 0,
          action: {
            label: 'I Got It',
            callback: this.dismissNotify.bind(this),
          },
        });
  }

  dismissNotify(notification){
    api.dismissAlert(this.state.biowall.serialnumber)()
      .then(response => {
        console.log(response.payload);
        notification.removeNotification(notification.uid);
      })
      .catch(err => {
        console.log('Failed to dismiss alert');
        console.log(err.toString());
      });
  }

  pollBioWalls() {
    api.getBioWalls()
      .then(response => this.handleBioWallApiResponse(response))
      .catch(err => {
        clearTimeout(this.state.timeoutId);
        if (!this.mounted) return;
        this.setState({
          errorText: err.toString(),
          biowall: null,
          timeoutId: null,
        })
      });
  }

  handleBioWallApiResponse(response) {
    if (!this.mounted) return;
    const timeoutId = setTimeout(() => this.pollBioWalls(), 1000);
    const { serialnumber } = this.props.params;
    const biowalls = response.payload;
    // console.log(biowalls);
    // console.log(this.state.biowall);
    const biowall = biowalls.filter(wall => wall.serialnumber == serialnumber)[0];
    if (!biowall) return hashHistory.replace('/biowalls');
    if (response.status === 304)
      this.setState({
        errorText: null,
        timeoutId
      });
    else{
      this.setState({
        errorText: null, biowall, timeoutId
      });
    }
  }

  /* called when we know there's an error message. It includes a little bit of
   * presentation but don't tell anybody */
  renderErrorMessage() {
    return (
      <div className="container container--space">
        <MessageBox error text={this.state.errorText} />
      </div>
    );
  }

  handleToggle(e){
    e.preventDefault();
    this.setState({b_listview: !this.state.b_listview});
  }

  handleGotoSolenoid(e){
    let url = "/biowall/" + this.state.biowall.serialnumber + "/output/Solenoid";
    e.preventDefault();
    if ((new Date).getTime() - this.state.start_time > 1000)
      this.props.router.push(url);
    else
      console.log('Ghost click is detected, ignoring...');
  }

  renderMainContent() {
    const { errorText, biowall } = this.state;
    if (!biowall) return <LoadingIndicator />;
    if (errorText) return this.renderErrorMessage();

    let columns = 3;
    if (window.innerWidth < 500) columns = 1;
    else if (window.innerWidth < 800) columns = 2;

    let solenoid = parseFloat(biowall.Solenoid);
    let light = parseFloat(biowall.Light);
    let fertilizer = parseFloat(biowall.Fertilizer);
    let temperature = parseFloat(biowall.Temperature);
    let ph = parseFloat(biowall.PH);
    let ec = parseFloat(biowall.EC);
    let leak = parseFloat(biowall.Leak);
    let pressure = parseFloat(biowall.Pressure);
    let flow = parseFloat(biowall.Flow);
    let level = parseFloat(biowall.Level);
    let moisture = parseFloat(biowall.Moisture);

    return (
      <BioWallDetail
        name = {biowall.name}
        sn={biowall.serialnumber}
        solenoid={solenoid}
        light={light}
        fertilizer={fertilizer}
        temperature={temperature}
        ph={ph}
        ec={ec}
        leak={leak}
        pressure={pressure}
        flow={flow}
        level={level}
        moisture={moisture}
        columns = {columns}
        b_listview={this.state.b_listview}
        onChangeToggle={(e) => this.handleToggle(e)}
        goto_solenoid={(e) => this.handleGotoSolenoid(e)}
      />
    );
  }

  render() {
    return (
      <div>
        <NavBar showHomeButton />
        {this.renderMainContent()}
        <NotificationSystem ref="notificationSystem"/>
      </div>
    );
  }
}

export default withRouter(BioWallView);

BioWallView.propTypes = {
  router: React.PropTypes.shape({
    push: React.PropTypes.func.isRequired
  }).isRequired
};