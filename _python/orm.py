import os

from peewee import *
from base import Base


db = SqliteDatabase(os.path.dirname(os.path.realpath(__file__)) + '/' + Base().get_param_from_xml('DB_NAME'))
db.connect()


class TBGateway(Model):
    """
    ORM class for Gateway Table.
    Because we have not specified a primary key,
    peewee will automatically add an auto-incrementing integer primary key field named id.
    """
    name = TextField()
    value = TextField()

    class Meta:
        database = db


class TBModbus(Model):
    """
    ORM class for Modbus Table
    """
    name = TextField()
    value = IntegerField()

    class Meta:
        database = db


class TBSensor(Model):
    """
    ORM class for Sensor Table
    """
    alias = TextField()             # Name of this sensor
    address = IntegerField()        # Modbus address
    adc_appr = FloatField()         # ADC approximation value
    # range = FloatField()            # Range to convert an analog signal to a digital one
    slope = FloatField()            # Slope
    intercept = FloatField()
    low_setpoint = TextField()     # Low Setpoint, Low Setpoint Alert Time
    high_setpoint = TextField()    # High Setpoint, High Setpoint Alert Time

    class Meta:
        database = db


class TBOutput(Model):
    """
    ORM class for Output Table
    """
    alias = TextField()
    address = IntegerField()
    days = TextField()          # List of which days to run, must be separaged with comma. e.g.: Mon/Tue/Sat
    start_time = TimeField()    # Start time field
    stop_time = TimeField()     # Stop time field
    cycle = IntegerField()      # Dictates how long the total timer cycle is.  Unit: second
    delay = IntegerField()      # Dictates how long of a delay should be made to call a function.  Unit: second

    class Meta:
        database = db


class TBZone(Model):
    """
    ORM class for Zone Table
    """
    name = TextField()
    start_address = IntegerField()      # Modbus starting address of this Zone
    sensors = TextField()               # Sensor list of zone, must be separated with comma.  e.g. : pH,EC,Moisture
    outputs = TextField()               # Output list of zone, must be separated with comma.  e.g. : Pump,Solenoid

    class Meta:
        database = db
