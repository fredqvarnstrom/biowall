"""
PWD: FurbP@ss
"""
import random
import threading
import time
import traceback
import platform
import sys
import calendar
import datetime
import peewee
import setup
from orm import *
import utils
import logging.config

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
if not os.path.exists(cur_dir + 'logs'):
    os.mkdir(cur_dir + 'logs')

from modbus_ctrl import ModbusCtrl
from murano_ctrl import MuranoCtrl

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except IOError:
    print 'Failed to load configuration of logging'

logger = logging.getLogger("biowall")


class BioWall(Base):

    murano = None
    modbus = None

    b_stop_sensing = False
    b_stop_output = False
    output_status = dict()
    alerts = dict()

    def __init__(self):
        Base.__init__(self)
        self.murano = MuranoCtrl(product_id=self.get_value_from_db('Gateway', 'Product_id'),
                                 identifier=self.get_value_from_db('Gateway', 'Identifier'),
                                 device_cik=self.get_value_from_db('Gateway', 'Device_cik'),
                                 long_poll_timeout=int(self.get_param_from_xml('LONG_POLL_REQUEST_TIMEOUT')))

        self.modbus = ModbusCtrl(self.get_param_from_xml('MODBUS_PORT'),
                                 baudrate=self.get_value_from_db('Modbus', 'Baudrate'))

    def activate_murano(self):
        """
        Activate Murano service
        :return:
        """
        status, msg = self.murano.activate()

        # If this is the 1st time of activation, store cik value to db
        if status:
            row_cik = TBGateway.get(TBGateway.name == 'Device_cik')
            row_cik.value = msg
            try:
                row_cik.save()
                return True
            except peewee.OperationalError:
                traceback.print_exc()
                logger.error('Failed to update CIK value: {}'.format(msg))
                return False
        elif status is None:
            return False
        elif not status:    # Device is already activated...
            return True

    @staticmethod
    def get_value_from_db(table_name, field_name):
        """
        :param table_name:
        :param field_name:
        :return:
        """
        if table_name == 'Gateway':
            try:
                val = TBGateway.get(TBGateway.name == field_name).value
                return val
            except TBGateway.DoesNotExist:
                logger.error('No data in Gateway table with given name, query: {}'.format(field_name))
                return None
        elif table_name == 'Modbus':
            try:
                val = TBModbus.get(TBModbus.name == field_name).value
                return val
            except TBModbus.DoesNotExist:
                logger.error('No data in Modbus table with given name, query: {}'.format(field_name))
                return None

    def run(self):
        """
        Starts 2 services
        :return:
        """
        threading.Thread(target=self.execute_sensing).start()
        for op in TBOutput.select():
            threading.Thread(target=self.execute_output, args=(op, )).start()
        threading.Thread(target=self.check_manual_output).start()

    def execute_output(self, output):
        """
        Execute given output...
        :return:
        """
        while True:
            if self.b_stop_output:
                time.sleep(.1)
            else:
                # Download timer of this output and update before executing
                self.update_timer_reference(output)
                # Update output instance
                output = TBOutput.get(TBOutput.alias == output.alias)

                # If sensor data is abnormal(Leak is detected or level is out of scope, or so on...), stop output
                if not self.check_sensor_before_output(output=output):
                    self.modbus.write_holding_register(output.address, 0)
                    time.sleep(10)
                    continue

                # Skip this output if today is not includes in its 'Days' list
                week_day = calendar.day_name[datetime.date.today().weekday()]
                if week_day[:3] not in output.days.split('/'):
                    logger.info('Today is out of schedule for {}'.format(output.alias))
                    time.sleep(10)
                    continue

                # Check whether current time is after start_time and is before stop_time
                cur_time = datetime.datetime.now()
                cur_sec = cur_time.hour * 3600 + cur_time.minute * 60 + cur_time.second
                start_sec = output.start_time.hour * 3600 + output.start_time.minute * 60 + output.start_time.second
                stop_sec = output.stop_time.hour * 3600 + output.stop_time.minute * 60 + output.stop_time.second
                if start_sec <= cur_sec <= stop_sec:
                    logger.info('Turning {} on...'.format(output.alias))
                    self.modbus.write_holding_register(output.address, 1)
                    time.sleep(output.delay)
                    logger.info('Turning {} off...'.format(output.alias))
                    self.modbus.write_holding_register(output.address, 0)
                    time.sleep(output.cycle - output.delay)
                else:
                    logger.info('This time is out of schedule for {}'.format(output.alias))
                    time.sleep(5)

    def update_timer_reference(self, output):
        """
        Download timer of this output and update
        :return:
        """
        status, resp = self.murano.read_from_murano(output.alias + '_timer')
        if status:
            try:
                # Sample response: Days=Mon/Tue/Fri,Start=08:00,Stop=21:00,Cycle=60,Delay=20
                timer_val = resp[(len(output.alias + '_timer') + 1):]        # Remove timer alias
                val_list = timer_val.split(',')
                b_update = False
                for val in val_list:
                    field_name = val.split('=')[0]
                    field_value = val.split('=')[1]
                    if field_name.lower() == 'days':
                        if not utils.validate_days_value(field_value):
                            logger.error('Invalid type of "Days" value on Murano, val: {}'.format(field_value))
                            continue
                        if output.days != field_value:
                            output.days = field_value
                            b_update = True
                    elif field_name.lower() == 'start':
                        try:
                            tmp = datetime.datetime.strptime(field_value, "%H:%M").time()
                        except ValueError:
                            logger.error('Invalid type of "Start Time" value on Murano, val: {}'.format(field_value))
                            continue
                        if output.start_time != tmp:
                            output.start_time = tmp
                            b_update = True
                    elif field_name.lower() == 'stop':
                        try:
                            tmp = datetime.datetime.strptime(field_value, "%H:%M").time()
                        except ValueError:
                            logger.error('Invalid type of "Stop Time" value on Murano, val: {}'.format(field_value))
                            continue
                        if output.stop_time != tmp:
                            output.stop_time = tmp
                            b_update = True
                    elif field_name.lower() == 'cycle':
                        try:
                            tmp = int(field_value)
                        except ValueError:
                            logger.error(
                                'Invalid type of "Cycle" value on Murano, must be integer, val: {}'.format(field_value))
                            continue
                        if output.cycle != tmp:
                            output.cycle = tmp
                            b_update = True
                    elif field_name.lower() == 'delay':
                        try:
                            tmp = int(field_value)
                        except ValueError:
                            logger.error(
                                'Invalid type of "Delay" value on Murano, must be integer, val: {}'.format(field_value))
                            continue
                        if output.delay != tmp:
                            output.delay = tmp
                            b_update = True
                if b_update:
                    try:
                        output.save()
                        logger.info(
                            'Updated the timer of {} successfully, new value: {}'.format(output.alias, timer_val))
                    except peewee.OperationalError:
                        traceback.print_exc()
                        logger.exception('Failed to update new timer value: {}'.format(output.alias))

            except IndexError:
                if len(resp.strip()) == 0:
                    logger.info('Seems like Timer Reference of {} is not set yet, uploading...'.format(output.alias))

                    new_timer = 'Days={},Start={},Stop={},Cycle={},Delay={}'.format(
                        output.days, output.start_time.strftime("%H:%M"),
                        output.stop_time.strftime("%H:%M"),
                        output.cycle, output.delay)

                    status, resp = self.murano.upload_to_murano(output.alias + '_timer=' + new_timer)
                    if status:
                        logger.info('Successfully uploaded, value: {}'.format(new_timer))
                    else:
                        logger.error('Failed to upload local data to Exosite, response: {}'.format(resp))
                else:
                    logger.error("Wrong response from Exosite: {}".format(resp))

    def check_manual_output(self):
        """
        Get all output status and update local status list.
        If any change is detected, execute it.
        :return:
        """
        while True:
            status, resp = self.murano.read_from_murano('&'.join([op.alias for op in TBOutput.select()]))
            if status:
                val_list = resp.split('&')
                for val in val_list:
                    try:
                        [op_name, op_val] = val.split('=')
                    except ValueError:
                        logger.error('Seems like outputs are not uploaded yet, response: {}'.format(resp))
                        continue

                    if op_val is None:
                        logger.info("An output({}) is updated, but its value is none".format(op_name))
                        continue

                    orm_op = TBOutput.get(TBOutput.alias == op_name)
                    try:
                        if self.output_status[op_name] != op_val:
                            self.output_status[op_name] = op_val
                            logger.info('New output value arrived, Output: {}, val: {}'.format(op_name, op_val))
                            if self.check_sensor_before_output(orm_op) or op_val == '0':
                                self.modbus.write_holding_register(orm_op.address, int(op_val))

                    except KeyError:
                        self.output_status[op_name] = op_val
                        logger.info('New output value arrived, Output: {}, val: {}'.format(op_name, op_val))
                        try:
                            op_val = int(op_val)
                        except ValueError:
                            logger.error('Invalid type of output value, should be "1" or "0": {}'.format(op_val))
                        if self.check_sensor_before_output(orm_op):
                            self.modbus.write_holding_register(orm_op.address, op_val)
                time.sleep(1)      # It'd be better to give 1 sec of delay
            else:
                logger.error('Failed to get output status from Murano...')
                time.sleep(.5)

    def execute_sensing(self):
        """
        Execute sensor logic
        :return:
        """
        for sensor in TBSensor.select():
            self.alerts[sensor.alias] = dict()
            self.alerts[sensor.alias]['high'] = 0
            self.alerts[sensor.alias]['low'] = 0

        while True:
            if self.b_stop_sensing:
                time.sleep(.1)
            else:
                # s_time = time.time()
                self.check_sensor_params()
                self.upload_sensor_output_data()
                # while time.time() - s_time < 10:
                #     time.sleep(.1)

    def check_sensor_params(self):
        """
        Check exosite to see if any user editable parameters on exosite had been changed and update db.
        :return:
        """
        sensor_list = [str(s.alias) for s in TBSensor.select()]
        for sensor in sensor_list:
            sp_list = [sensor + '_high_setpoint', sensor + '_low_setpoint']
            for sp in sp_list:
                status, resp = self.murano.read_from_murano(sp)
                if status:
                    # Parse parameters and update DB if any value is changed...
                    # Sample response: 'Flow_high_setpoint=500.0' or ''
                    if resp.strip() == '' or ',' not in resp:
                        if 'high' in sp:
                            sp_val = TBSensor.get(TBSensor.alias == sensor).high_setpoint
                        else:
                            sp_val = TBSensor.get(TBSensor.alias == sensor).low_setpoint
                        logger.info(
                            'Seems like {} is not set on Murano, uploading local value to it: {}'.format(sp, sp_val))
                        self.murano.upload_to_murano(sp + '=' + str(sp_val))
                    else:
                        try:
                            new_val = resp.split('=')[1]
                            row = TBSensor.get(TBSensor.alias == sensor)
                            if 'high' in sp:
                                if new_val != row.high_setpoint:
                                    logger.info('New High Setpoint of {} is detected: {}'.format(sensor, new_val))
                                    row.high_setpoint = new_val
                                    try:
                                        row.save()
                                    except peewee.OperationalError:
                                        traceback.print_exc()
                                        logger.error('Failed to update High Set Point of {}'.format(sensor))
                            else:
                                if new_val != row.low_setpoint:
                                    logger.info('New Low Setpoint of {} is detected: {}'.format(sensor, new_val))
                                    row.low_setpoint = new_val
                                    try:
                                        row.save()
                                    except peewee.OperationalError:
                                        traceback.print_exc()
                                        logger.exception('Failed to update Low Set Point of {}'.format(sensor))
                        except IndexError:
                            logger.error('Wrong response from Murano, request: {}, response: {}'.format(sp, resp))
                        except ValueError:
                            logger.error('Wrong response from Murano, request: {}, response: {}'.format(sp, resp))
                else:
                    logger.error('Failed to receive data from exosite, response: {}'.format(resp))

    def upload_sensor_output_data(self):
        """
        Read sensor/output data from modbus controller and upload to murano.
        :return:
        """
        request_list = []

        for sen in TBSensor.select():
            read_data = self.read_sensor_data(sen)
            if read_data is not None:
                request_list.append(sen.alias + '=' + str(read_data))

        for out in TBOutput.select():
            if platform.system() == 'Windows':      # When running on my Windows PC
                read_data = random.randint(0, 1)
            else:
                read_data = self.modbus.read_holding_register(out.address)
            if read_data is not None:
                request_list.append(out.alias + '=' + str(read_data))

        logger.debug('Uploading data: {}'.format(request_list))
        if request_list is not None:
            status, resp = self.murano.upload_to_murano('&'.join(request_list))
            if status:
                # print 'Uploaded successfully, response: ', resp
                pass
            else:
                logger.error('Failed to upload data to murano, response: {}'.format(resp))

    @staticmethod
    def check_update():
        """
        Check the latest version of current script and download/reboot if new.
        :return:
        """
        # TODO: Implement logic of checking the latest version of script.
        return True

    def check_sensor_before_output(self, output):
        """
        Check sensor status before executing output
        :param output:
        :return:
        """
        # Currently we are applying rules to the water pump only
        if output.alias == 'Solenoid':
            status, resp = self.murano.read_from_murano("Leak_high_setpoint")
            if not status:
                logger.error("Failed to download Leak Threshold from Exosite, ignoring...")
                return True
            try:
                sp = resp.split('=')[1]
                thr = float(sp.split(',')[0])
                leak = TBSensor.get(TBSensor.alias == 'Leak')
                leak_val = self.read_sensor_data(leak)
                if leak_val is not None:
                    if leak_val < thr:
                        return True
                    else:
                        logger.warning("Warning! Leak is detected!")
                        return False
                else:
                    return True
            except IndexError:
                logger.warning('Seems like Leak Threshold is not set yet, ignoring...')
                return True
            except ValueError:
                logger.warning('Seems like Leak Threshold is invalid({}), ignoring...'.format(resp))
                return True

        elif output.alias == 'Level':
            status, resp = self.murano.read_from_murano("Level_high_setpoint&Level_low_setpoint")
            if not status:
                logger.warning("Failed to download Level Setpoints from Exosite, ignoring...")
                return True
            level_val = self.read_sensor_data(TBSensor.get(TBSensor.alias == 'Level'))
            if level_val is None:
                logger.warning('Failed to read level sensor value, ignoring....')
                return True

            high_sp = low_sp = None
            for setpoint in resp.split('&'):
                try:
                    sp = setpoint.split('=')[1]
                    if setpoint.split('=')[0] == 'Level_high_setpoint':
                        high_sp = float(sp.split(',')[0])
                    elif setpoint.split('=')[0] == 'Low_high_setpoint':
                        low_sp = float(sp.split(',')[0])
                except IndexError:
                    logger.warning('Seems like Level Setpoint is not set yet, ignoring...')
                except ValueError:
                    logger.warning('Seems like Level Setpoint is invalid({}), ignoring...'.format(resp))

            if high_sp is not None:
                if level_val > high_sp:
                    logger.warning("Warning! Level is higher than high_setpoint!")
                    return False
            if low_sp is not None:
                if level_val < low_sp:
                    logger.warning("Warning! Level is lower than low_setpoint!")
                    return False
            return True

        else:
            return True

    def read_sensor_data(self, sensor):
        """
        Read data from modbus device, convert its value, return
        :param sensor: TBSensor instance
        :return:
        """
        if platform.system() == 'Windows':  # When running on my Windows PC
            read_data = random.randint(1, 100)
        else:
            read_data = self.modbus.read_holding_register(sensor.address)
        if read_data is None:
            return None
        bits = TBModbus.get(TBModbus.name == 'Register_bit_length').value
        # Convert this data to a meaningful value with adc_appr, range, slope, intercept
        read_data = round(read_data * sensor.adc_appr / (2 ** bits) * sensor.slope + sensor.intercept, 3)

        # Compare with High Setpoint and Low Setpoint
        [high_sp, alert_h] = sensor.high_setpoint.split(',')
        if read_data <= float(high_sp):
            # Initialize alert value of this sensor
            self.alerts[sensor.alias]['high'] = 0
        else:
            # This is the 1st time of alert
            if self.alerts[sensor.alias]['high'] == 0:
                if int(alert_h) == 0:   # If High Alert value is set as 0, send alert immediately
                    self.send_alert(alert='high', sensor=sensor, value=read_data, duration=0)
                else:
                    self.alerts[sensor.alias]['high'] = time.time()
            # Compare with Alert value
            elif time.time() - self.alerts[sensor.alias]['high'] > int(alert_h) * 60:
                self.send_alert(alert='high', sensor=sensor, value=read_data, duration=alert_h)

        [low_sp, alert_l] = sensor.low_setpoint.split(',')
        if read_data >= float(low_sp):
            # Initialize alert value of this sensor
            self.alerts[sensor.alias]['low'] = 0
        else:
            # This is the 1st time of alert
            if self.alerts[sensor.alias]['low'] == 0:
                if int(alert_l) == 0:  # If Low Alert value is set as 0, send alert immediately
                    self.send_alert(alert='low', sensor=sensor, value=read_data, duration=0)
                else:
                    self.alerts[sensor.alias]['low'] = time.time()
            # Compare with Alert value
            elif time.time() - self.alerts[sensor.alias]['low'] > int(alert_l) * 60:
                self.send_alert(alert='low', sensor=sensor, value=read_data, duration=alert_l)

        return read_data

    def send_alert(self, alert, sensor, value, duration):
        """
        Send alert to user and initialize self.alert variable to prevent sending too many alerts
        :param duration:
        :param alert: Alert Type, `high` or `low`
        :param sensor:  TBSensor instance
        :param value:   Current reading value
        :return:
        """
        msg = "Warning, the {} value at {} has been {}er than {}-setpoint for {}, current value is {}.\n" \
              "Please check quickly!" \
              "".format(sensor.alias,
                        TBGateway.get(TBGateway.name == 'Device_alias').value,
                        alert,
                        alert,
                        duration,
                        value)

        logger.warning(msg)
        # Send alert message!
        self.murano.upload_to_murano('alert=' + msg)
        self.alerts[sensor.alias][alert] = 0


if __name__ == '__main__':

    logger.info(' ====== Starting BioWall Backend script... ======')
    print 'For general configurations, please check "config.xml" file.\n'

    # Start setup script
    setup.startup()

    b = BioWall()
    print 'Starting main script.... debug: {}'.format(b.debug)

    if not b.activate_murano():
        logger.error("Failed to activate to Murano service.")
        sys.exit(1)

    b.check_update()
    b.run()
    # print b.murano.read_from_murano('Temperature')
    # print b.murano.read_from_murano('Temperature_low_setpoint')
