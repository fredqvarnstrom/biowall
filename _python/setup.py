"""
Setup script for BioWall devices
In order to add/remove sensors/outputs, please open `config.xml` and add/remove to the SENSORS/OUTPUTS tag.
"""
import datetime
from orm import *
from base import Base
import utils
import logging.config

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except IOError:
    print 'Failed to load configuration of logging'

logger = logging.getLogger("biowall")
prompt = '> '
sensor_list = Base().get_param_from_xml('SENSORS').split(',')
output_list = Base().get_param_from_xml('OUTPUTS').split(',')


def gateway_config():
    while True:
        print "We will begin with Preparing the Gateway Device Connection. Please respond to every prompt. \n"

        print "What is the Alias of your gateway? (must be a Unique wall name)"
        device_alias = raw_input(prompt)

        print "What is the Product ID? (from exosite murano)"
        product_id = raw_input(prompt)

        print "What is your Device Identifier? (from exosite)"
        serial_number = raw_input(prompt)

        print "Is the everything correct (y/n)? \n Device_alias %r \n Product ID= %r \n Serial number= %r \n" % \
              (device_alias, product_id, serial_number)
        if raw_input(prompt) == 'y':
            if not TBGateway.table_exists():
                TBGateway.create_table()
            name_list = ['Device_alias', 'Product_id', 'Identifier', 'Device_cik']
            val_list = [device_alias, product_id, serial_number, '']
            for i in range(len(name_list)):
                TBGateway.create(name=name_list[i], value=val_list[i])

            logger.info(" Gateway table updated with:\n Device_alias %r\n Product ID= %r\n Identifier= %r \n" % (
                device_alias, product_id, serial_number))
            break
        print "let's try again."


def modbus_config():
    while True:
        print "We will begin with Preparing the Modbus Device Connection. Please respond to every prompt. \n"
        l_prompt = [
            "What is the Modbus Device Address (0-255?)",
            "What is the Modbus Device Baud rate?  Usually 9600 or 19200.",
            "What is the register bit length for your input registers? (usually 8 or 12)",
        ]
        l_name_field = [
            'Modbus_address',
            'Baudrate',
            'Register_bit_length',
        ]
        try:
            # Get input values and compose a list of them
            val_list = []
            for i in range(len(l_prompt)):
                print l_prompt[i]
                val_list.append(int(raw_input(prompt)))

            print "Is everything correct (y/n)? \n Modbus_address = %r \n Baudrate = %r \n " \
                  "Register_bit_length = %r" % tuple(val_list)
            if raw_input(prompt) == 'y':
                # Insert rows into the Modbus Table
                if not TBModbus.table_exists():
                    TBModbus.create_table()
                for i in range(len(l_prompt)):
                    TBModbus.create(name=l_name_field[i], value=val_list[i])

                logger.info("Modbus table updated with: \n Modbus_address = %r \n Baudrate = %r \n "
                            "Register_bit_length = %r " % tuple(val_list))
                break
            else:
                print "Let's try again."
        except ValueError:
            print 'Error, invalid input value. Please try again.'
            continue


def sensors_config():
    print "We will begin with Preparing the Sensors for your device. \n"
    # Get input values and compose a list of them
    i = 0
    while i < len(sensor_list):
        try:
            print 'Configuring {} Sensor::: '.format(sensor_list[i])
            print "Modbus register address:"
            addr = int(raw_input(prompt))

            print "ADC approximation value:"
            adc_appr = float(raw_input(prompt))

            # print "Converting range: "
            # c_range = float(raw_input(prompt))

            print 'Slope:'
            slope = float(raw_input(prompt))

            print 'Intercept:'
            intercept = float(raw_input(prompt))

            print 'Low Setpoint value:'
            low_st = float(raw_input(prompt))

            print 'Low Setpoint Alert Time(min):'
            alert_l = int(raw_input(prompt))

            print 'High Setpoint:'
            high_st = float(raw_input(prompt))

            print 'High Setpoint Alert Time(min):'
            alert_h = int(raw_input(prompt))

            print "Is everything correct (y/n)?"
            # txt = "::: %r :::\n Address: %r\n ADC Approximation: %r\n Converting Range: %r\n Slope: %r\n" \
            txt = "::: %r :::\n Address: %r\n ADC Approximation: %r\n Slope: %r\n" \
                  " Intercept: %r\n Low Setpoint: %r\n  Low Setpoint Alert Time: %r\n High Setpoint: %r  " \
                  "High Setpoint Alert Time: %r" % \
                  (sensor_list[i], addr, adc_appr, slope, intercept, low_st, alert_l, high_st, alert_h)
            # (sensor_list[i], addr, adc_appr, c_range, slope, intercept, low_st, alert_l, high_st, alert_h)
            print txt
            if raw_input(prompt) == 'y':
                if not TBSensor.table_exists():
                    TBSensor.create_table()
                TBSensor.create(alias=sensor_list[i], address=addr, adc_appr=adc_appr, slope=slope,
                                intercept=intercept, low_setpoint='{},{}'.format(low_st, alert_l),
                                high_setpoint='{},{}'.format(high_st, alert_h))
                txt = 'Sensor is updated with ' + txt.replace('\n', ',')
                logger.info(txt)
                i += 1
            else:
                print 'Let us try again.'
        except ValueError:
            print 'Error, invalid input value. Please try again.'


def outputs_config():
    print "We will begin with Preparing the Outputs for your device. \n"
    i = 0
    while i < len(output_list):
        try:
            # Get input values and compose a list of them
            output = output_list[i]
            print "What is the modbus register address of your {}?".format(output)
            addr = int(raw_input(prompt))

            print 'Which days will {} run?  e.g. Mon/Tue/Sat/Sun'.format(output)
            days = raw_input(prompt)
            if not utils.validate_days_value(days):
                print 'Invalid input of days value, please try again'
                continue

            print 'When will {} start timer?   e.g. 07:30'.format(output)
            str_start_time = raw_input(prompt)
            start_time = datetime.datetime.strptime(str_start_time, "%H:%M")

            print 'When will {} stop timer?   e.g. 21:20'.format(output)
            str_stop_time = raw_input(prompt)
            stop_time = datetime.datetime.strptime(str_stop_time, "%H:%M")

            print "How long is the total cycle time of {} in seconds?".format(output)
            cycle = int(raw_input(prompt))

            print "How long is the duration time of {} in seconds?".format(output)
            delay = int(raw_input(prompt))

            print "Is everything correct (y/n)?"
            txt = "::: %r :::\n Address: %r\n Working Days: %r\n Start Time: %r\n Stop Time: %r\n Cycle: %r\n " \
                  "Delay: %r" % (output, addr, days, str_start_time, str_stop_time, cycle, delay)
            print txt
            if raw_input(prompt) == 'y':
                if not TBOutput.table_exists():
                    TBOutput.create_table()
                TBOutput.create(alias=output, address=addr, days=days, start_time=start_time, stop_time=stop_time,
                                cycle=cycle, delay=delay)
                logger.info("Output is updated with " + txt.replace('\n', ','))
                i += 1
            else:
                print "Let's try again."
        except ValueError:
            print 'Error, invalid input value. Please try again.'
            continue


def zone_config():
    print "We will begin with Preparing the Zones for your device. \n"

    while True:
        try:
            print 'What is the name of Zone?'
            zone_name = raw_input(prompt)

            print 'What is the starting modbus address of {}'.format(zone_name)
            start_addr = int(raw_input(prompt))

            # Attach sensors
            l_sensors = []
            while True:
                print 'Would you like to add sensor? (y/n)'
                if raw_input(prompt) == 'y':
                    print 'What is the sensor name to be added?'
                    sensor_name = raw_input(prompt)
                    try:
                        TBSensor.get(TBSensor.name == sensor_name)
                        l_sensors.append(sensor_name)
                    except TBSensor.DoesNotExist:
                        print 'Cannot find sensor, try again.'
                else:
                    break

            # Attach outputs
            l_output = []
            while True:
                print 'Would you like to add output? (y/n)'
                if raw_input(prompt) == 'y':
                    print 'What is the output name to be added?'
                    output_name = raw_input(prompt)
                    try:
                        TBOutput.get(TBOutput.name == output_name)
                        l_output.append(output_name)
                    except TBOutput.DoesNotExist:
                        print 'Cannot find output, try again.'
                else:
                    break

            print 'Is everything correct for {}? (y/n)'.format(zone_name)
            txt = '::: %r :::\n Starting address: %r\n Sensors: %r\n Outputs: %r\n' % \
                  (zone_name, start_addr, ','.join(l_sensors), ','.join(l_output))
            if raw_input(prompt) == 'y':
                TBZone.create(name=zone_name, start_address=start_addr, sonsors=','.join(l_sensors),
                              outputs=','.join(l_output))
                logger.info("Zone is updated with " + txt.replace('\n', ','))

            print 'Would you like to add another zone? (y/n)'
            if raw_input(prompt) == 'y':
                continue
            else:
                break

        except ValueError:
            print 'Error, invalid input value. Please try again.'
            continue


def startup():
    """
    Check all tables and prompt user to config empty tables...
    Setup sequence:
        Gateway -> Modbus -> Sensor -> Output
    :return:
    """
    print " --- Starting setup script ---"
    while True:
        if not TBGateway.table_exists() or len(TBGateway.select()) == 0:
            b_gateway = False
        else:
            b_gateway = True
        if not TBModbus.table_exists() or len(TBModbus.select()) == 0:
            b_modbus = False
        else:
            b_modbus = True
        if not TBSensor.table_exists() or len(TBSensor.select()) == 0:
            b_sensor = False
        else:
            b_sensor = True
        if not TBOutput.table_exists() or len(TBOutput.select()) == 0:
            b_output = False
        else:
            b_output = True

        print '\n ... Checking tables in database ...'
        print "Gateway : %r, Modbus : %r, Sensors : %r, Outputs : %r" % (
            b_gateway, b_modbus, b_sensor, b_output)

        if b_gateway and b_modbus and b_sensor and b_output:
            print 'All tables are configured, starting main script...'
            return True

        raw_input("Press Enter to start configuration.")
        print "What would you like to configure? (1=Gateway, 2=Modbus, 3=Sensors, 4=Outputs)"
        try:
            selection = int(raw_input(prompt))
        except ValueError:
            print "Error, please input numbers only."
            continue

        if selection == 1:
            if b_gateway:
                print 'Would you like to reset Gateway table? (y/n)'
                if raw_input(prompt) == 'y':
                    TBGateway.delete().execute()
                    gateway_config()
            else:
                gateway_config()
        elif selection == 2:
            if b_modbus:
                print "Would you like to reset Modbus table? (y/n)"
                if raw_input(prompt) == 'y':
                    TBModbus.delete().execute()
                    modbus_config()
            else:
                modbus_config()
        elif selection == 3:
            if b_sensor:
                print 'Would you like to reset Sensor table? (y/n)'
                if raw_input(prompt) == 'y':
                    TBSensor.delete().execute()
                    sensors_config()
            else:
                sensors_config()
        elif selection == 4:
            if b_output:
                print 'Would you like to reset Output table? (y/n)'
                if raw_input(prompt) == 'y':
                    TBOutput.delete().execute()
                    outputs_config()
            else:
                outputs_config()


if __name__ == '__main__':
    startup()
