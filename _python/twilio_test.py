# Download the Python helper library from twilio.com/docs/python/install
from twilio.rest import TwilioRestClient

# Your Account Sid and Auth Token from twilio.com/user/account
account_sid = "AC60f41bd72c537356a6d0508d04be1883"
auth_token = "c7bff0912a72ec4977ffb08ee5e16e07"
client = TwilioRestClient(account_sid, auth_token)

message_sid = "SMefed7022a5874e9b885b6f06978135cf"

client.messages.redact(message_sid)
message = client.messages.get(message_sid)
print message.body  # Will be an empty string

client.messages.delete(message_sid)
